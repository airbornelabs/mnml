<!DOCTYPE html>
<!--[if IE 9]><html class="lt-ie10" lang="en" > <![endif]-->
<html class="no-js" lang="en" >
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="<?php bloginfo('description');?>">
  <title><?php bloginfo('name');?></title>

  <link rel="stylesheet" href="<?php bloginfo('template_directory');?>/assets/stylesheets/app.css">

  <script src="<?php bloginfo('template_directory');?>/assets/bower_components/modernizr/modernizr.js"></script>
</head>
<body>
<header class="full-width">
	<div class="row">
		<div class="large-12 medium-12 small-12 columns text-center">
      <h1><a href="<?php bloginfo('url');?>" title="<?php bloginfo('name');?>">Anthony Terrell</a></h1>
			<h2><?php echo quotes();?></h2>
		</div>
	</div>
</header>

<div class="full-width content-wrap">
  
