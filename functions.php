<?php 

function quotes(){
	$quotes = array(
		'Software Engineer',
		'Beer Aficionado',
		'Professional Procrastinator',
		'Native New Yorker',
		'A Hip Hop Head',
		'Home Brewer',
		'Recovering Peanut Butter Enthusiast'
		);
	shuffle($quotes);
	return $quotes[0];
}
